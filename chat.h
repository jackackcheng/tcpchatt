#ifndef __CHAT_H__
#define __CHAT_H__

#include<stdlib.h>
#include<stdio.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<string.h>
#include<netinet/in.h>
#include<fcntl.h>
#include<pthread.h>

#define IP  "172.27.29.217"//内网ip
#define PIP   "226.62.188.114"//公网ip
#define PORT 3389//端口 
#define MSGSIZE 128//消息大小
#define NAMESIZE 15//姓名长度

typedef struct User
{
        char name[NAMESIZE];//姓名
        char msg[MSGSIZE];//消息
}User;//用户信息

typedef struct DoubleListNode
{
        int value;
        struct DoubleListNode*prev;
        struct DoubleListNode*next;
}DLNode;//带头双向链表

DLNode*CreateNode(int value);//创建节点
void PushFront(DLNode*phead,int value);//头插节点
void Erase(DLNode*phead,int value);//删除节点

#endif//__CHAT_H__
