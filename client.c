#include "chat.h"

int encrypt_mm(char *plainT,int mm,char *cipherT){
    int i = 0;
    int p_len = strlen(plainT);
    char encrypt_cipher[128] = {0};
    for(i=0;i< p_len;i++){
        if(i < p_len/2){
            encrypt_cipher[i] = *(plainT + i) + mm;
        }else{
            encrypt_cipher[i] = *(plainT + i) - mm;
        }
    }
    encrypt_cipher[p_len] = '\0';
    strcpy(cipherT,encrypt_cipher);
}

int decrypt_mm(char *cipherT,char mm,char *plainT)
{
    int i = 0;
    int c_len = strlen(cipherT);
    char decrypt_plain[128] = {0};
    for(i=0;i<c_len;i++)
    {
        if(i < c_len/2){
            decrypt_plain[i] = *(cipherT + i) - mm;
        }else{
            decrypt_plain[i] = *(cipherT + i) + mm;
        }
    }
    decrypt_plain[c_len] = '\0';
    strcpy(plainT,decrypt_plain);
}

int SockDeal()//创建、连接套接字
{
        int sock=socket(AF_INET,SOCK_STREAM,0);
        if(sock<0)
        {
                perror("socket error");
                exit(1);
        }
        struct sockaddr_in server;
        server.sin_family=AF_INET;
        server.sin_port=htons(PORT);
        server.sin_addr.s_addr=inet_addr(PIP);

        if(connect(sock,(struct sockaddr*)&server,sizeof(server))<0)
        {
                perror("connet error");
                exit(2);
        }
        return sock;
}

void MsgDeal(int sock,char*name)//处理消息
{
        char buf[NAMESIZE+MSGSIZE]={0};
        User user;
        User encrypt_msg,decrypt_msg;

        while(1)//消息循环收发
        {
                fflush(stdout);
                memset(buf,0,NAMESIZE+MSGSIZE);
                memset(user.msg,0,MSGSIZE);
                memset(encrypt_msg.msg,0,MSGSIZE);
                fcntl(0,F_SETFL,FNDELAY);//sock置为非阻塞
                int read_size=read(0,user.msg,MSGSIZE);
                if(read_size>0)
                {
                        strcpy(buf,name);  
                        encrypt_mm(user.msg,5,encrypt_msg.msg);
                        strcpy(buf+NAMESIZE,encrypt_msg.msg);   
                        if(send(sock,buf,NAMESIZE+MSGSIZE,MSG_DONTWAIT)<0)
                        {
                                perror("send error");
                                exit(1);
                        }
                }
                int recv_size=recv(sock,buf,NAMESIZE+MSGSIZE,MSG_DONTWAIT);
                if(recv_size>0)
                {
                memset(user.name,0,NAMESIZE);
                memset(user.msg,0,MSGSIZE);
                memset(decrypt_msg.msg,0,MSGSIZE);
                strcpy(user.name,buf);
                strcpy(user.msg,buf+NAMESIZE);
                decrypt_mm(user.msg,5,decrypt_msg.msg);
                printf("%s 说: %s\n",user.name,decrypt_msg.msg);
                }
        } 
        close(sock);

}


int main(int argc,char *argv[])
{
        if(argc!=2)
        {
                perror("input format error");
                exit(1);
        }
        int sock=SockDeal();

        MsgDeal(sock,argv[1]);

        close(sock);
        return 0;
}
