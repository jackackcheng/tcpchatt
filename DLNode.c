#include "chat.h"

DLNode*CreateNode(int value)
{
        DLNode*node=(DLNode*)malloc(sizeof(DLNode));
        node->value=value;
        node->next=NULL;
        node->prev=NULL;
        return node;
}

void PushFront(DLNode*phead,int value)
{
        DLNode*node=CreateNode(value);
        if(phead->next==NULL)
        {
                phead->next=node;
                node->prev=phead;
        }
        else
        {
                node->next=phead->next;
                node->next->prev=node;
                phead->next=node;
                node->prev=phead;
        }
}

void Erase(DLNode*phead,int value)
{
        DLNode*cur=phead->next;
        while(cur!=NULL)
        {
                if(cur->value==value)
                {
                        DLNode*prev=cur->prev;
                        prev->next=cur->next;
                        if(cur->next!=NULL)
                                cur->next->prev=prev;
                        else
                                prev->next=NULL;
                        free(cur);
                        cur=NULL;
                }
                else
                {
                        cur=cur->next;
                }
        }
}
